import React from 'react';
import {NavLink} from 'react-router-dom'

function SignedLinks(){
    return(
       <ul className="right">
           <li><NavLink to='/'>content  </NavLink></li>
           <li><NavLink to='/'>careers </NavLink></li>
           <li><NavLink to='/' className='btn btn-floating pink lighten-1'>New Projet </NavLink></li>
       </ul>

    );

}

export default SignedLinks;