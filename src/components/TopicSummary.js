import React from 'react'

const TopicSummary=({project})=>{
    return(
        <div className="project-list section">
        <div className="card z-depth-0 project-summary">
    <span className="card-title">{project.title}</span>
            <p>Postec by Mr Bean</p>
            <p className="grey-text"> 3rd September</p>
    </div>
</div>
    )
}


export  default TopicSummary;