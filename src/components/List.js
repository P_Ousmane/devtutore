import React from 'react'
import TopicSummary from './TopicSummary'


const List =({projects})=>{

    return(
        <div>
        { projects && projects.map(project=>{
            return( <TopicSummary project={project} key={project.id}/>)
           
        })}

        </div>
       

     )
}


export default  List;