import React from 'react';
import {Link} from 'react-router-dom'
import SignedLinks from './SignedLinks'

function Navbar(){
    return(
       <nav className="nav-wrapper grey darken-3">
           <div className="container">
               <Link to='/' className="brand-logo center">DaaraJ Cars</Link>
               <SignedLinks/>

           </div>
       </nav>

    );

}

export default Navbar;