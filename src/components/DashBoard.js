import React,{Component} from 'react';
import List from './List'
import {connect} from 'react-redux'


class  DashBoard extends Component{
   
   render(){
      const {projects}= this.props;
    return(
        <div className="dashboard container">
            <div className="row">
                <div className=" col s12 m7">
                <List projects={projects}/>
                   

                </div>
                <div className="col s12 m4 offset-m1"></div>

            </div>
        </div>
    )

   }
   
   
   
    
}

const mapStateToProps=(state)=>{
    return {
        projects:state .project.projects
    }
}

export default connect(mapStateToProps)(DashBoard);