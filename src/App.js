import React from 'react';
import logo from './logo.svg';
import './App.scss';
import {BrowserRouter , Switch , Route} from 'react-router-dom'
import './components/Navbar'
import Navbar from './components/Navbar';
import DashBoard from './components/DashBoard'
import TopicDetails from './components/TopicDetails'
import SignIn from './components/Authentification/SignIn';


function App() {
  return (
    <BrowserRouter>
     <div className="App">
      <Navbar/>
      <Switch>
       <Route path='/' component={DashBoard}/> 
        <Route path='/' component={SignIn}/>
        

      </Switch>
      
    </div>
    </BrowserRouter>
   
  );
}

export default App;
