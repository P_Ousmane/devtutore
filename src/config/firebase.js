import firebase from 'firebase/app'
import 'firebase/firestore';
import 'firebase/auth'

var firebaseConfig = {
    apiKey: "AIzaSyDNYkmfFW2pu3ses0t5EBPWM0mmpXOJ8GI",
    authDomain: "daraajcarz.firebaseapp.com",
    databaseURL: "https://daraajcarz.firebaseio.com",
    projectId: "daraajcarz",
    storageBucket: "daraajcarz.appspot.com",
    messagingSenderId: "730745311791",
    appId: "1:730745311791:web:d1e7eadf1dd704dc636cda",
    measurementId: "G-5LJ08K2W60"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.firestore().settings({timestampsInSnapshots:true});

  export default firebaseConfig;